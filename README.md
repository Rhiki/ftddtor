# README #

A repo for files and small scripts, that don't deserve their own repo.

## List of scripts:

### [/bash/screenshot-active-window](./bash/screenshot-active-window)
Takes a screenshot of the currently active window, saves it to `~/Pictures/Screenshots/<WINDOW_NAME>_<CURRENT_DATE_TIME>.png` and copies the image to the clipboard.

---
### [/bash/screenshot-select-area](./bash/screenshot-active-window)
Takes a screenshot of a user selected rectangle, saves it to `~/Pictures/Screenshots/<CURRENT_DATE_TIME>.png` and copies the image to the clipboard.